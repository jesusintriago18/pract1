package com.example.movil.practica1BailonIntriago;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.movil.practica1BailonIntriago.R;

public class MainActivity extends AppCompatActivity {

    Button botonLogin, botonRegistrar, botonBuscar,botonParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonLogin = (Button) findViewById(R.id.btnlogin);
        botonRegistrar = (Button) findViewById(R.id.btnguardar);
        botonBuscar = (Button) findViewById(R.id.btnbuscar);
        //botonParametro = (Button) findViewById(R.id.btnParametro1);
        botonParametro = (Button)findViewById(R.id.buttonPasarParametro);

botonParametro.setOnClickListener(new View.OnClickListener(){
    @Override
    public void onClick(View view){
        Intent intent = new Intent(MainActivity.this, PasarParametroActivity.class);
        startActivity(intent);
    }
    });


        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);
            }
        });
        botonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                startActivity(intent);
            }
        });
        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Main4Activity.class);
                startActivity(intent);
            }
        });

    }
}
